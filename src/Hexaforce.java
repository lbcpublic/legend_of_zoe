/**
 * Classe de l'item "Hexaforce qui déclenche le gain d'hexaforces de Zoé lorsqu'il est récolté.
 */
public final class Hexaforce extends Consommable {
    @Override
    public void activer() {
        persPrinc.gagnerHexaforce();
    }
}
