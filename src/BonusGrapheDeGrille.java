import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Extension de la classe BonusGraphe qui rajoute la notion de distance et l'algorithme A*.
 */
public class BonusGrapheDeGrille extends BonusGraphe<Integer[]> {
    /**
     * Distance euclidienne sur une grille planaire.
     * @param labelDebut Label du sommet de départ.
     * @param labelFin Label du sommet d'arrivée.
     * @return Distance euclidienne entre les deux sommets.
     */
    public double distance(int labelDebut, int labelFin){
        if(labelDebut == labelFin){
            return 0.0;
        }

        Integer[] posDebut = this.getSommet(labelDebut).contenu;
        Integer[] posFin = this.getSommet(labelFin).contenu;

        return Math.sqrt(Math.pow(posFin[0] - posDebut[0],2) + Math.pow(posFin[1] - posDebut[1],2));
    }

    /**
     * Méthode interne : Distance euclidienne sur une grille planaire.
     * @param sommetDebut Sommet de départ.
     * @param sommetFin Sommet d'arrivée.
     * @return Distance euclidienne entre les deux sommets.
     */
    private double distance(Sommet sommetDebut, Sommet sommetFin){
        Integer[] posDebut = sommetDebut.contenu;
        Integer[] posFin = sommetFin.contenu;

        return Math.sqrt(Math.pow(posFin[0] - posDebut[0],2) + Math.pow(posFin[1] - posDebut[1],2));
    }

    /**
     * Implémentation de l'algorithme A*
     * https://en.wikipedia.org/wiki/A*_search_algorithm#Pseudocode
     * @param labelDebut Label du sommet de départ
     * @param labelFin Label du sommet d'arrivée
     * @return Tableau de tableaux de Integer, chacun contenant le label du sommet ([0]) et sa position (x:[1],y:[2])
     */
    public Integer[][] aStar(int labelDebut, int labelFin){
        Sommet sommetDebut = this.getSommet(labelDebut);
        Sommet sommetFin = this.getSommet(labelFin);

        ArrayList<Integer[]> chemin = new ArrayList<>();
        ArrayList<Sommet> ensOuvert = new ArrayList<>();
        ArrayList<Sommet> ensFerme = new ArrayList<>();
        Hashtable<Sommet,Double> cout = new Hashtable<>();
        Hashtable<Sommet,Double> longChemin = new Hashtable<>();
        Hashtable<Sommet,Sommet> precedent = new Hashtable<>();
        Hashtable<Sommet,Double> distVersFin = new Hashtable<>();
        boolean nouveau;

        ensOuvert.add(sommetDebut);
        longChemin.put(sommetDebut,0.0);
        distVersFin.put(sommetDebut,this.distance(sommetDebut,sommetFin));
        cout.put(sommetDebut,distVersFin.get(sommetDebut));

        while(ensOuvert.size() > 0){
            // Trouve le sommet au coût minimal et le retire de l'ensemble ouvert.
            Sommet minSommet = null;
            double minDist = Double.POSITIVE_INFINITY;
            int minI = 0;

            for(int i = 0; i < ensOuvert.size(); i++){
                Sommet sommet = ensOuvert.get(i);
                double dist = cout.get(sommet);

                if(dist < minDist){
                    minSommet = sommet;
                    minDist = dist;
                    minI = i;
                }
            }

            if(minSommet == sommetFin){
                break;
            }

            ensFerme.add(ensOuvert.remove(minI));

            for(Arete arete : minSommet.aretes){
                Sommet voisin = arete.getVoisin(minSommet);

                nouveau = true;
                for(Sommet sommet : ensFerme){
                    if(sommet == voisin){
                        nouveau = false;
                        break;
                    }
                }

                if(nouveau){
                    for(Sommet sommet : ensOuvert){
                        if(sommet == voisin){
                            nouveau = false;
                            break;
                        }
                    }
                }else{continue;}

                double nouvLong = longChemin.get(minSommet) + arete.poids;

                if(nouveau){
                    ensOuvert.add(voisin);
                    precedent.put(voisin,minSommet);

                    distVersFin.put(voisin,this.distance(voisin,sommetFin));
                    longChemin.put(voisin,nouvLong);
                    cout.put(voisin,nouvLong + distVersFin.get(voisin));

                }else if(longChemin.get(voisin) > nouvLong){
                    precedent.put(voisin,minSommet);

                    longChemin.put(voisin,nouvLong);
                    cout.put(voisin,nouvLong + distVersFin.get(voisin));
                }
            }
        }

        Sommet prev = sommetFin;

        if(precedent.containsKey(sommetFin)){
            while(prev != null){
                Integer[] ele = new Integer[3];

                ele[0] = prev.label;
                ele[1] = prev.contenu[0];
                ele[2] = prev.contenu[1];

                chemin.add(ele);
                prev = precedent.get(prev);
            }

            Integer[][] ret = new Integer[chemin.size()][];

            for(int i = chemin.size() - 1; i >= 0; i--){
                ret[chemin.size() - 1 - i] = chemin.get(i);
            }

            return ret;
        }else{return null;}
    }
}
