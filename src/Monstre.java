/**
 * Classe qui décrit la fabrication et le comportement des monstres du jeu.
 * On lui associe un item et, lorsque le jeu est en mode "bonus", un attribut intelligent.
 */
public final class Monstre extends Personnage {
    private boolean intelligent;
    private Paire<Consommable, String> infoLot;
    private String nom;

    /**
     * Constructeur de l'instance du monstre.
     * @param pos Position [X,Y] du monstre sur la grille.
     * @param numNivActu Le niveau du tableau. (Sa vie et ses dommages en dépendent.)
     * @param intelligent True si le jeu a été lancé avec l'option "bonus".
     * @param infoLot L'item contenu dans l'instance du monstre.
     */
    public Monstre(int[] pos, int numNivActu, boolean intelligent,
                   Paire<Consommable, String> infoLot, String nom){
        super((int)(0.6*numNivActu) < 1 ? 1 : (int)(0.6*numNivActu),
                (int)(0.4*numNivActu) < 1 ? 1 : (int)(0.4*numNivActu));
        this.pos[0] = pos[0];
        this.pos[1] = pos[1];
        this.intelligent = intelligent;
        this.infoLot = new Paire<>(infoLot.getKey(),infoLot.getValue());
        this.nom = nom;
    }

    /**
     * Déplacement du monstre. Varie selon le mode de jeu.
     * @param zoeX Position en X de Zoé.
     * @param zoeY Position en Y de Zoé.
     */
    public void bouger(int zoeX, int zoeY){
        int dX = zoeX - this.pos[0];
        dX = dX > 0 ? 1 : (dX == 0 ? 0 : -1);

        int dY = zoeY - this.pos[1];
        dY = dY > 0 ? 1 : (dY == 0 ? 0 : -1);

        if(dX != 0 || dY != 0){
            if(this.intelligent) {
                Integer[] newPos = lvl.getChemin(this.pos[0],this.pos[1],zoeX,zoeY);

                if(newPos != null){
                    this.pos[0] = newPos[1];
                    this.pos[1] = newPos[2];
                }else if(lvl.estLibre(this.pos[0] + dX, this.pos[1] + dY)){
                    this.pos[0] += dX;
                    this.pos[1] += dY;
                }
            }else{
                if(lvl.estLibre(this.pos[0] + dX, this.pos[1] + dY)){
                    this.pos[0] += dX;
                    this.pos[1] += dY;
                }
            }
        }
    }

    /**
     * Attaque Zoé lorsqu'elle est à proximité.
     */
    public void attaquer(){
        Zoe persPrinc = lvl.getZoe(this.pos[0], this.pos[1]);

        if(persPrinc != null){
            lvl.ajMessage(this.nom + " attaque Zoé!\n");
            persPrinc.subirDegats(this.degats);
        }
    }

    /**
     * Libère le lot du monstre et le fait mourrir.
     */
    public void mourrir(){
        lvl.ajMessage(this.nom + " est K.O!\n");
        lvl.ajMessage(this.infoLot.getValue());
        this.infoLot.getKey().activer();
        this.infoLot = null;
    }

    /**
     * "Getter" qui retourne le nom du monstre.
     * @return Le nom du monstre.
     */
    public String getNom(){
        return this.nom;
    }
}