/**
 *Classe des coffres au trésor du jeu. Contient 1 item et peut être soit fermé, soit ouvert.
 */
public final class Coffre extends Positionnable {
    private Paire<Consommable,String> infoLot;
    private boolean ouvert;

    /**
     * Constructeur du coffre.
     * @param pos Sa position [X,Y] dans la grille du niveau.
     * @param infoLot L'item contenu.
     */
    public Coffre(int[] pos, Paire<Consommable,String> infoLot){
        this.pos[0] = pos[0];
        this.pos[1] = pos[1];
        this.infoLot = new Paire<>(infoLot.getKey(), infoLot.getValue());
        this.ouvert = false;
    }

    /**
     * Ouverture du coffre déclenchée par l'action de Zoé.
     * Déclenche l'effet du contenu ainsi que son message associé.
     */
    public void ouvrir(){
        this.ouvert = true;
        lvl.ajMessage(this.infoLot.getValue());
        this.infoLot.getKey().activer();
        this.infoLot = null;
    }

    /**
     * "Getter" du statut ouvert ou fermé du coffre.
     * @return True s'il est ouvert. False sinon.
     */
    public boolean estOuvert(){
        return this.ouvert;
    }
}
