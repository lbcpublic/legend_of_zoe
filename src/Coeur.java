/**
 * Classe de l'item "coeur" qui redonne 1 vie à Zoé lorsqu'il est récolté.
 */
public final class Coeur extends Consommable {
    @Override
    public void activer(){
        persPrinc.restaurer(1);
    }
}
