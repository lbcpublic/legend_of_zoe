/**
 * Classe parente des items du jeu qui assigne l'effet futur des items du jeu au personnage de Zoé lors
 * de l'initialisation de la partie.
 */
public abstract class Consommable {
    protected static Zoe persPrinc;

    public abstract void activer();

    /**
     * Initialise le personnage principal au début du jeu.
     * @param persPrinc Zoé
     */
    public static final void setPersPrinc(Zoe persPrinc){

        if(Consommable.persPrinc == null){
            Consommable.persPrinc = persPrinc;
        }
    }
}
