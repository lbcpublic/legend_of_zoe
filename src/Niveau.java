import java.util.Arrays;

/**
 * Classe qui contient toute l'information du niveau actuel de la partie:
 *     La carte du niveau.
 *     Zoé
 *     Un tableau des monstres.
 *     Un tableau des coffres.
 *     La position de la sortie.
 *     Le numéro du niveau.
 *     Les condiitons suffisantes pour changer de niveau.
 *     Les messages.
 *
 */
public final class Niveau {
    private boolean[][] plan;
    private static Zoe persPrinc;
    private Monstre[] monstres;
    private Coffre[] coffres;
    private int[] posSortie;
    private int numNiv;
    private boolean complete;
    private String messages;
    private int[][] idSommets;
    private BonusGrapheDeGrille grapheDeGrille;
    private BonusGraphe<Integer[]>[] sousGraphes;
    private boolean bonus;
    private static String[] nomsDeMonstres = new String[]{"Nicolas",
                                                          "Alena",
                                                          "Abdelhakim",
                                                          "Samuel",
                                                          "Laurent",
                                                          "Raphaël"};

    /**
     * Constructeur de niveau.
     * @param plan La carte du niveau ("Vraie" pour l'emplacement d'un mur, "Faux" pour une case vide.)
     * @param contenu Les objets contenus dans le niveau.
     * @param numNiv Le numéro du niveau à construire.
     * @param monstresIntelligents True si le jeu a été lancé en mode "bonus".
     */
    public Niveau(boolean[][] plan, String[] contenu, int numNiv, boolean monstresIntelligents){
        this.messages = "";

        this.numNiv = numNiv;

        this.complete = false;
        this.bonus = monstresIntelligents;

        int maxY = plan.length;
        int maxX = plan[0].length;

        this.plan = new boolean[maxY][maxX];

        for(int i = 0; i < maxY; i++){
            for(int j = 0; j < maxX; j++){
                this.plan[i][j] = plan[i][j];
            }
        }

        Monstre[] tmpMonstres = new Monstre[contenu.length];
        int ajTmpMonstres = 0;
        Coffre[] tmpCoffres = new Coffre[contenu.length];
        int ajTmpCoffres = 0;

        boolean[] nomsPris = new boolean[6];

        // Décortique le contenu généré par LevelGenerator.
        for(String ele : contenu){
            String[] info = ele.split(":");

            int[] pos = new int[2];

            if(info.length == 3){
                pos[0] = Integer.parseInt(info[1]);
                pos[1] = Integer.parseInt(info[2]);
            }else{
                pos[0] = Integer.parseInt(info[2]);
                pos[1] = Integer.parseInt(info[3]);
            }

            if(info[0].equals("tresor")){
                Paire<Consommable, String> infoLot = defLot(info[1]);

                Coffre tmp = new Coffre(pos,infoLot);

                tmpCoffres[ajTmpCoffres] = tmp;

                ajTmpCoffres++;
            }else if(info[0].equals("monstre")){
                Paire<Consommable, String> infoLot = defLot(info[1]);

                boolean rempli = true;
                for(int i = 0; i < 6; i++){
                    rempli = rempli && nomsPris[i];
                }

                if(rempli){
                    for(int i = 0; i < 6; i++){
                        nomsPris[i] = false;
                    }
                }

                int nom = (int)(6.0*Math.random());

                while(nomsPris[nom]){
                    nom = (int)(6.0*Math.random());
                }

                nomsPris[nom] = true;
                Monstre tmp = new Monstre(pos,this.numNiv,
                        monstresIntelligents,infoLot, nomsDeMonstres[nom]);

                tmpMonstres[ajTmpMonstres] = tmp;

                ajTmpMonstres++;
            }else if(info[0].equals("sortie")){
                this.posSortie = pos;
            }else if(info[0].equals("zoe")){
                persPrinc.setPos(pos);
            }else{
                throw new ExceptionInInitializerError("No match " +
                        "for description string (" + info[0] + ")");
            }
        }

        // Enregistre le contenu décortiqué dans les attributs de la classe.
        this.monstres = new Monstre[ajTmpMonstres];

        for(int i = 0; i < ajTmpMonstres; i++){
            this.monstres[i] = tmpMonstres[i];
        }

        this.coffres = new Coffre[ajTmpCoffres];

        for(int i = 0; i < ajTmpCoffres; i++){
            this.coffres[i] = tmpCoffres[i];
            int[] posCoffre = tmpCoffres[i].getPos();
        }

        //Évite de créer la grille si le bonus n'est pas activé.
        if(monstresIntelligents) {
            this.grapheDeGrille = new BonusGrapheDeGrille();
            this.idSommets = new int[plan.length][plan[0].length];

            for (int i = 0; i < plan.length; i++) {
                for (int j = 0; j < plan[i].length; j++) {
                    this.idSommets[i][j] = -1;
                }
            }

            for (int i = 0; i < plan.length; i++) {
                for (int j = 0; j < plan[0].length; j++) {
                    if (!this.plan[i][j]) {
                        if(this.estLibre(j,i)){
                            this.creerSommet(j, i);
                        }
                    }
                }
            }

            this.sousGraphes = this.grapheDeGrille.composantesConnexes();
        }
    }

    /**
     * Bonus: Crée un sommet du graphe en position x, y et le connecte à ses voisins.
     * @param x Position x du sommet.
     * @param y Position y du sommet.
     */
    private void creerSommet(int x, int y){
        this.idSommets[y][x] = this.grapheDeGrille.creerSommet(new Integer[]{x,y});

        int[] sommetsProches = this.trouvSommetsGrille(x,y);
        for(int i = 0; i < 8; i++){
            if(sommetsProches[i] > -1){

                // Sommets connectés dans les 8 directions.
                if(!this.grapheDeGrille.connectes(this.idSommets[y][x],
                        sommetsProches[i])){
                    this.grapheDeGrille.creerArete(1.0,
                            this.idSommets[y][x],sommetsProches[i],false);
                }
            }
        }

    }

    /**
     * Bonus: Retourne les sommets autour de la position (x,y).
     * @param x position x du sommet.
     * @param y position y du sommet.
     * @return Un tableau contenant les labels des sommets environnants.
     */
    private int[] trouvSommetsGrille(int x, int y){
        int[] ret = new int[8];

        Arrays.fill(ret,-1);

        if(x > 0){
            ret[0] = this.idSommets[y][x-1];
        }
        if(x > 0 && y > 0){
            ret[1] = this.idSommets[y-1][x-1];
        }
        if(y > 0){
            ret[2] = this.idSommets[y-1][x];
        }
        if(x < this.idSommets[0].length-1 && y > 0){
            ret[3] = this.idSommets[y-1][x+1];
        }
        if(x < this.idSommets[0].length-1){
            ret[4] = this.idSommets[y][x+1];
        }
        if(x < this.idSommets[0].length-1 && y < this.idSommets.length-1){
            ret[5] = this.idSommets[y+1][x+1];
        }
        if(y < this.idSommets.length-1){
            ret[6] = this.idSommets[y+1][x];
        }
        if(x > 0 && y < this.idSommets.length-1){
            ret[7] = this.idSommets[y+1][x-1];
        }

        return ret;
    }

    /**
     * Permet d'instancier et d'associer un item à un objet
     * qui contient un item dans la construction du niveau.
     * @param info représente l'item contenu dans l'objet.
     * @return l'instance de l'item associé au message qui sera affiché
     *         lorsque Zoé le récoltera.
     */
    private static Paire<Consommable, String> defLot(String info){
        Consommable lot = null;
        String message = "";

        if(info.equals("coeur")){
            lot = new Coeur();
            message = "Vous trouvez un coeur!\n";
        }else if(info.equals("potionvie")){
            lot = new PotionVie();
            message = "Vous trouvez une potion de vie!\n";
        }else if(info.equals("hexaforce")){
            lot = new Hexaforce();
            message = "Vous trouvez un morceau d'Hexaforce!\n";
        }

        return new Paire<>(lot,message);
    }

    /**
     * Intègre Zoé dans les niveaux.
     * @param persPrinc L'instance de Zoé initialisée au début de la partie.
     */
    public static void setPersPrinc(Zoe persPrinc){
        Niveau.persPrinc = persPrinc;
    }

    /**
     *Détermine si l'emplacement est libre pour un déplacement.
     * @param x position en x de la carte.
     * @param y position en y de la carte.
     * @return True si la case est libre.
     */
    public boolean estLibre(int x, int y){
        if(x >= this.plan[0].length || y >= this.plan.length || x < 0 || y < 0) {
            return false;
        }else{
            for(Coffre coffre : this.coffres){
                int[] pos = coffre.getPos();

                if(pos[0] == x && pos[1] == y){
                    return false;
                }
            }

            if(this.posSortie[0] == x && this.posSortie[1] == y){
                return false;
            }

            return !this.plan[y][x];
        }
    }

    /**
     * Détermine s'il y a des coffres fermés dans les 8 cases
     * autour de la position [x,y] entrée en paramètre.
     * @param x Position x de la carte.
     * @param y Position y de la carte.
     * @return Un tableau contenant les coffres fermés autour de la position.
     */
    public Coffre[] getCoffres(int x, int y){
        Coffre[] tmp = new Coffre[this.coffres.length];

        int posInsert = 0;

        for(Coffre candidat : this.coffres){
            int[] candPos = candidat.getPos();

            if((Math.abs(candPos[0] - x) <= 1 && Math.abs(candPos[1] - y) <= 1)
                    && !candidat.estOuvert()){
                tmp[posInsert] = candidat;
                posInsert++;
            }
        }

        Coffre[] coffresProches = new Coffre[posInsert];

        for(int i = 0; i < posInsert; i++){
            coffresProches[i] = tmp[i];
        }

        return coffresProches;
    }

    /**
     * Vérifie la présence de monstres vivants dans les 8 cases
     * autour d'une position [x,y] du tableau.
     * @param x Coordonnée en x de la carte.
     * @param y Coordonnée en y de la carte.
     * @return Un tableau de monstres vivants autour de la positions [x,y].
     */
    public Monstre[] getMonstres(int x, int y){
        Monstre[] tmp = new Monstre[this.monstres.length];

        int posInsert = 0;

        for(Monstre candidat : this.monstres){
            if(candidat.enVie()){
                int[] candPos = candidat.getPos();

                if(Math.abs(candPos[0] - x) <= 1 && Math.abs(candPos[1] - y) <= 1){
                    tmp[posInsert] = candidat;
                    posInsert++;
                }
            }
        }

        Monstre[] monstresProches = new Monstre[posInsert];

        for(int i = 0; i < posInsert; i++){
            monstresProches[i] = tmp[i];
        }

        return monstresProches;
    }

    /**
     * Vérifie la présence de Zoé dans les 8 cases autour de la position [x,y].
     * @param x Coordonnée en x.
     * @param y Coordonnée en y.
     * @return Zoé si elle est présente.
     */
    public Zoe getZoe(int x, int y){
        int [] zoePos = persPrinc.getPos();

        if ((Math.abs(zoePos[0] - x) <= 1) && (Math.abs(zoePos[1] - y) <= 1)){
            return persPrinc;
        }else{return null;}
    }

    /**
     * Bonus: Trouve le chemin le plus court entre la
     * position initiale (x,y) et la position visée.
     * @param xDeb Position initiale en x.
     * @param yDeb Position initiale en y.
     * @param xFin Position de la cible en x.
     * @param yFin Position de la cible en y.
     * @return Le id et la position du prochain sommet
     *         que le monstre devra suivre sur le chemin.
     */
    public Integer[] getChemin(int xDeb, int yDeb, int xFin, int yFin){
        if(this.bonus){
            int idDep = this.idSommets[yDeb][xDeb];
            int idFin = this.idSommets[yFin][xFin];

            // Vérifie si la cible et la position initiale sont dans
            // la même composante connexe du graphe.
            if(this.sousGraphes.length > 1){
                for(int i = 0; i < this.sousGraphes.length; i++){
                    boolean aDepart = this.sousGraphes[i].aPourSommet(idDep);
                    boolean aFin = this.sousGraphes[i].aPourSommet(idFin);

                    if((aDepart && !aFin) || (!aDepart && aFin)){
                        return null;
                    }
                }
            }

            // Calcule le chemin.
            Integer[][] chemin = this.grapheDeGrille.aStar(idDep,idFin);

            if(chemin != null){
                return chemin[1];
            }else{return null;}
        }else{
            return null;
        }
    }

    /**
     * Action des monstres vivants. Ils se déplacent vers
     * Zoé et attaquent. Les monstres n'existent pas
     * à l'extérieur de leur niveau. Le niveau s'occupe donc des monstres.
     */
    public void jouerMonstres(){
        int[] zoePos = persPrinc.getPos();

        Thread[] joueurs = new Thread[this.monstres.length];

        for(int i = 0; i < this.monstres.length; i++){
            Monstre actuel = this.monstres[i];
            JoueurDeMonstre joueur = new JoueurDeMonstre(actuel,zoePos);
            Thread t = new Thread(joueur);

            t.start();

            joueurs[i] = t;
        }

        for(Thread t : joueurs){
            try{
                t.join();
            }catch (InterruptedException e){
                System.out.println("Thread " + t + " interrupted.");
            }
        }
    }

    /**
     * Remplace les murs dans les 8 cases qui encerclent
     * la position [x,y] par des cases vides sur lesquelles
     * il est possible de se déplacer.
     * Bonus: Si un mur a été supprimé, les composantes connexes
     * du graphe sont recalculées.
     * @param x Coordonnée x.
     * @param y Coordonnée y.
     */
    public void detruireMurs(int x, int y){
        boolean aDetruit = false;

        // Zoé détruit jusqu'à 8 murs. Vérifie si au moins 1 a été détruit.
        aDetruit = this.detruireCase(x-1,y) || aDetruit;
        aDetruit = this.detruireCase(x-1,y-1) || aDetruit;
        aDetruit = this.detruireCase(x,y-1) || aDetruit;
        aDetruit = this.detruireCase(x+1,y-1) || aDetruit;
        aDetruit = this.detruireCase(x+1,y) || aDetruit;
        aDetruit = this.detruireCase(x+1,y+1) || aDetruit;
        aDetruit = this.detruireCase(x,y+1) || aDetruit;
        aDetruit = this.detruireCase(x-1,y+1) || aDetruit;

        // Pas besoin de recalculer les composantes connexes s'il n'y en a qu'une.
        if(this.bonus && this.sousGraphes.length > 1 && aDetruit){
            this.sousGraphes = this.grapheDeGrille.composantesConnexes();
        }
    }

    /**
     * Détruit une case spécifique.
     * @param x Position x de la case à détruire.
     * @param y Position y de la case à détruire.
     * @return Si la case a été détruite.
     */
    private boolean detruireCase(int x, int y){
        if(!(x < 0 || x >= this.plan[0].length || y < 0 || y >= this.plan.length)){
            if(this.plan[y][x]){
                this.plan[y][x] = false;
                if(this.bonus){
                    this.creerSommet(x,y);
                }
                return true;
            }
            return false;
        }else{return false;}
    }


    /**
     * Établit les conditions nécessaires pour passer au niveau suivant.
     * Zoé sur la sortie et avoir récolté l'hexaforce
     * du niveau.
     * @param x Coordonnée x de la carte.
     * @param y Coordonnée y de la carte.
     */
    public void verifFin(int x, int y){
        if(Math.abs(x - this.posSortie[0]) <= 1
                && Math.abs(y - this.posSortie[1]) <= 1
                && persPrinc.aHexaforce()){
            this.complete = true;
        }
    }

    /**
     * "Getter qui indique si le niveau est complet.
     * @return
     */
    public boolean estComplete(){return this.complete;}

    public void ajMessage(String message){this.messages += message;}

    /**
     * Affichage sur la console du niveau.
     * @return Un message qui indique le nb de vie de Zoé
     * sous forme de coeur ainsi que le nb d'hexaforces en sa
     * possession suivi d'un "newline" et d'un tableau
     * de char[][] qui permet de distinguer les objets.
     *       &: Zoé
     *       _: Coffre ouvert
     *       $: Coffre fermé
     *       E: Sortie
     *       @: Monstre vivant
     *       x: Monstre mort
     *       #: Mur
     */
    @Override
    public String toString(){
        int maxX = this.plan[0].length;
        int maxY = this.plan.length;

        int[] pos;

        char[][] affichage = new char[maxY][maxX];

        for(int i = 0; i < maxY; i++){
            for(int j = 0; j < maxX; j++){
                affichage[i][j] = this.plan[i][j] ? '#' : ' ';
            }
        }

        for(Coffre coffre : this.coffres){
            pos = coffre.getPos();

            affichage[pos[1]][pos[0]] = coffre.estOuvert() ? '_' : '$';
        }

        affichage[this.posSortie[1]][this.posSortie[0]] = 'E';

        for(Monstre monstre : this.monstres){
            pos = monstre.getPos();

            affichage[pos[1]][pos[0]] = monstre.enVie() ? '@' : 'x';
        }

        pos = persPrinc.getPos();

        affichage[pos[1]][pos[0]] = '&';

        String ret = this.messages;

        // Barre de vie et d'hexaforce.
        int life = persPrinc.getVie();
        int hexa = persPrinc.getHexaforce();

        ret += "Zoe: ";

        for(int i = 0; i < 5; i++){
            // Coeur
            ret += i < life ? "\u2665 " : "_ ";
        }

        ret += '|';

        for(int i = 0; i < 6; i++){
            ret += i < hexa ? "\u0394 " : "_ ";
        }

        ret += "\n";

        for(int i = 0; i < maxY; i++){
            for(int j = 0; j < maxX; j++){
                ret += affichage[i][j];
            }
            ret += '\n';
        }

        this.messages = "";

        return ret;
    }
}