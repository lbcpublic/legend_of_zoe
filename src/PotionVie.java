/**
 * Classe de l'item potion qui remlit au complet la vie de Zoé lorsqu'elle est récoltée.
 */
public final class PotionVie extends Consommable {
    @Override
    public void activer() {
        System.out.println(persPrinc.getVieMax());
        persPrinc.restaurer(persPrinc.getVieMax());
    }
}
