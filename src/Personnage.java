/**
 * Classe parente des objets qui ont des points de vie et qui font des dégâts.
 */
public abstract class Personnage extends Positionnable {
    private Object vieLock;
    private int vie;
    private int vieMax;
    protected int degats;
    private boolean enVie;
    private static boolean godMode = false;

    /**
     * Constructeur du personnage.
     * @param vieMax Son nombre maximum de vies.
     * @param degats Les dommages qu'il inflige.
     */
    public Personnage(int vieMax, int degats){
        this.vieLock = new Object();
        this.vie = vieMax;
        this.vieMax = vieMax;
        this.degats = degats;
        this.enVie = true;
    }

    public abstract void bouger(int dX, int dY);
    public abstract void attaquer();
    public abstract void mourrir();

    /**
     * "Getter" du nombre de vies de l'objet.
     * @return le nombre de vies.
     */
    public final int getVie(){
        return this.vie;
    }

    /**
     * Assigne un nombre de vie à l'objet. Le nombre de vies
     * assignées ne peut pas être supérieur au nombre maximal.
     * @param n Nombre de vies assignées.
     */
    protected final void setVie(int n){
        if(this.enVie && n > 0){
            if(n <= this.vieMax){
                this.vie = n;
            }else{
                this.vie = this.vieMax;
            }
        }
    }

    /**
     * "Getter" du nombre de vie maximal de l'objet.
     * @return le nombre de vies maximal.
     */
    public final int getVieMax(){
        return this.vieMax;
    }

    /**
     * "Setter" du nombre de vie maximal d'un objet.
     * Principalement utilisé lors de la création d'un niveau.
     * @param nouvVieMax Vie maximale voulue pour l'objet.
     */
    protected final void setVieMax(int nouvVieMax){
        this.vieMax = nouvVieMax;
    }

    /**
     * Calcul de la vie restante de l'objet selon la quantité
     * de dommages reçus. Nul pour Zoé dans le mode "--god".
     * @param n Quantité de dommages reçus.
     */
    public final void subirDegats(int n){
        synchronized (this.vieLock){
            if(this.enVie){
                if(this.vie - n <= 0){
                    this.vie = 0;
                    this.enVie = false;
                    this.mourrir();
                }else if(n > 0){
                    this.vie -= n;
                }
            }

            // Vie maximale pour Zoé suite à une attaque dans le mode "--God".
            if(godMode && this instanceof Zoe){
                this.enVie = true;
                this.setVie(this.vieMax);
            }
        }
    }

    /**
     * Indicateur de vie ou de mort de l'objet.
     * @return true s'il est en vie.
     */
    public final boolean enVie(){
        return this.enVie;
    }

    /**
     * Indicateur du mode de jeu "--god". True si le jeu a été lancé avec l'option "--god".
     */
    public static void godMode(){
        godMode = true;
    }
}