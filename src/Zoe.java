/**
 * Personnage principal du jeu. Ses méthodes correspondent aux actions
 * entrées au clavier dans le main.
 */
public final class Zoe extends Personnage {
    private int nHexaforce;
    private boolean aHexaforce;

    /**
     * Constructeur: Zoé est construite 1 fois au début de la partie
     * et a 5 vies et aucun hexaforce.
     */
    public Zoe(){
        super(5,1);
        this.nHexaforce = 0;
        this.degats = 1;
        this.aHexaforce = false;
    }

    /**
     * Vérifie si Zoé a l'hexaforce du niveau actuel.
     * @return Vrai si c'est le cas.
     */
    public boolean aHexaforce() {
        return this.aHexaforce;
    }

    /**
     * Réinitialise la valeur de aHexaforce lors de la fin du niveau actuel.
     */
    public void reinitHexaforce(){
        this.aHexaforce = false;
    }

    /**
     * Selon les directions entrées en argument, la position de Zoé est mise à jour
     * si c'est possible.
     * @param dX Direction du déplacement en X.
     * @param dY Direction du déplacement en Y.
     */
    public void bouger(int dX, int dY){
        if(dX <= 1 && dX >= -1 &&  dY <= 1 && dY >= -1 && (dX == 0 || dY == 0)){
            if(lvl.estLibre(this.pos[0] + dX, this.pos[1] + dY)){
                this.pos[0] += dX;
                this.pos[1] += dY;
                lvl.verifFin(this.pos[0], this.pos[1]);
            }
        }
    }

    /**
     * Inflige des dégâts à tous les monstres dans un rayon de une case autour de Zoé.
     */
    public void attaquer(){
        Monstre[] monstresProches = lvl.getMonstres(this.pos[0], this.pos[1]);

        for(Monstre cible : monstresProches){
            lvl.ajMessage("Zoé attaque " + cible.getNom() + "!\n");
            cible.subirDegats(this.degats);
        }
    }

    /**
     * Ouvre tous les coffres dans un rayon de une case autour de Zoé.
     * @return Boolean qui signifie au coffre qu'il est maintenant ouvert.
     */
    public void ouvrirCoffres(){
        Coffre[] coffresProches = lvl.getCoffres(this.pos[0], this.pos[1]);

        for(Coffre cible : coffresProches){
            cible.ouvrir();
        }
    }

    /**
     * Getter qui permet d'obtenir le nb d'Hexaforce de Zoé.
     * @return Le nombre d'Hexaforce de Zoé.
     */
    public int getHexaforce(){
        return this.nHexaforce;
    }

    /**
     * Enlève les murs des 8 cases autour de Zoé.
     * @return false pour signifier que les cases qui possédaient un mur sont maintenant vides.
     */
    public void creuser(){
        lvl.detruireMurs(this.pos[0], this.pos[1]);
    }

    /**
     * "Setter" qui établit le nb de vie de Zoé suite à un gain de vie.
     * @param n Le nombre de vie à rajouter.
     */
    public void restaurer(int n){
        this.setVie(this.getVie() + n);
    }

    /**
     * Mort de Zoé. Affiche l'écran de défaite.
     */
    public void mourrir(){
        lvl.ajMessage("Zoé meurt!\n");
        LegendOfZoe.defaite();
    }

    /**
     * Gain d'un hexaforce. Affiche l'écran de victoire dès que le sixième est obtenu.
     */
    public void gagnerHexaforce(){
        this.nHexaforce++;
        this.aHexaforce = true;

        if(this.nHexaforce == 6){
            LegendOfZoe.victoire();
        }
    }
}