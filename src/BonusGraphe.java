import java.util.ArrayList;

/**
 * Bonus: Classe générale de graphe dont les sommets ont un contenu paramétré.
 * Structure qui couvre le plan du niveau pour permettre le calcul du chemin.
 * @param <T> Type de contenu de chaque sommet.
 */
public class BonusGraphe <T>{
    private int prochainSommet = 0;
    private int prochaineArete = 0;

    /**
     * Sommet qui représente une case libre du plan du niveau.
     */
    protected class Sommet{
        protected int label;
        public T contenu;
        protected ArrayList<Arete> aretes;

        /**
         * Constructeur de sommet
         * @param contenu Le contenu du sommet.
         */
        public Sommet(T contenu){
            this.label = prochainSommet++;
            this.aretes = new ArrayList<>();
            this.contenu = contenu;
        }

        /**
         * Ajouter une arête connectée au sommet. Prévient une seconde référence à l'arête.
         * @param nouvArete L'arête à ajouter.
         */
        public void ajArete(Arete nouvArete){
            if(this.aretes.size() == 0){
                this.aretes.add(nouvArete);
            }else if(this.aretes.get(this.aretes.size()-1) != nouvArete){
                // Ceci vérifie si l'arête ne connecte pas le sommet à lui-même.
                this.aretes.add(nouvArete);
            }
        }

        /**
         * Supprimer une arête connectée au sommet.
         * @param aSupp Arête à supprimer.
         */
        public void supArete(Arete aSupp){
            for(int i = 0; i < this.aretes.size();i++){
                if(this.aretes.get(i) == aSupp){
                    this.aretes.remove(i);
                    aSupp.getVoisin(this).supArete(aSupp);
                    break;
                }
            }
        }

        /**
         * Obtenir l'étiquette d'un sommet.
         * @return L'étiquette.
         */
        public int getLabel(){
            return this.label;
        }

        /**
         * Destructeur explicite : supprime toutes ses références d'arête.
         */
        public void close(){
            int dernierIndex = this.aretes.size()-1;
            while(dernierIndex >= 0){
                this.aretes.remove(dernierIndex--).close();
            }
        }
    }

    /**
     * Classe interne qui relie 2 sommets.
     */
    protected class Arete {
        protected int label;
        protected double poids;
        protected Sommet sommetDebut;
        protected Sommet sommetFin;
        protected boolean oriente;

        /**
         * Constructeur d'arête.
         * @param poids  Le poids de l'arête (e.g. distance).
         * @param sommetDebut  Le sommet de départ.
         * @param sommetFin  Le sommet d'arrivée.
         * @param oriente  Si l'arête est orientée ou non.
         */
        public Arete(double poids, Sommet sommetDebut, Sommet sommetFin, boolean oriente){
            this.label = prochaineArete++;
            this.poids = poids;
            this.sommetDebut = sommetDebut;
            this.sommetFin = sommetFin;
            this.oriente = oriente;

            this.sommetDebut.ajArete(this);
            this.sommetFin.ajArete(this);
        }

        /**
         * Inversion de l'orientation d'une arête.
         */
        public void inverser(){
            if(this.oriente){
                Sommet tmp = this.sommetFin;
                this.sommetFin = this.sommetDebut;
                this.sommetDebut = tmp;
            }
        }

        /**
         * Vérifie si l'arête connecte deux sommets. Tient compte de l'orientation.
         * @param sommetDebut Sommet de départ.
         * @param sommetFin Sommet d'arrivée.
         * @return Si les sommets sont connectés ou non.
         */
        public boolean connecte(Sommet sommetDebut, Sommet sommetFin){
            if(oriente){
                return this.sommetDebut == sommetDebut && this.sommetFin == sommetFin;
            }else{
                return (this.sommetDebut == sommetDebut && this.sommetFin == sommetFin) ||
                        (this.sommetDebut == sommetFin && this.sommetFin == sommetDebut);
            }
        }

        /**
         * Retourne le voisin d'un sommet via une arête.
         * @param autre L'autre sommet.
         * @return Le voisin de l'autre sommet, s'il existe.
         */
        public Sommet getVoisin(Sommet autre){
            if(this.sommetDebut == autre){
                return this.sommetFin;
            }else if(this.sommetFin == autre){
                return this.sommetDebut;
            }else{
                throw new IllegalArgumentException("Cannot get the neighbouring Sommet of some" +
                        "Sommet that is not connected to an Arete!");
            }
        }

        /**
         * "Getter" qui permet d'obtenir l'étiquette d'une arête.
         * @return L'étiquette de l'arête.
         */
        public int getLabel(){
            return this.label;
        }

        /**
         * Destructeur explicite de l'arête. Supprime toutes les références vers les sommets qu'elle contient.
         */
        public void close(){
            this.sommetDebut.supArete(this);
            this.sommetDebut = null;
            this.sommetFin.supArete(this);
            this.sommetFin = null;
        }
    }

    protected ArrayList<Sommet> sommets;
    protected ArrayList<Arete> aretes;

    /**
     * Constructeur de graphe.
     */
    public BonusGraphe(){
        this.sommets = new ArrayList<>();
        this.aretes = new ArrayList<>();
    }

    /**
     * Crée un nouveau sommet avec un contenu et retourne son label.
     * @param contenu Le contenu du nouveau sommet.
     * @return Le label du nouveau sommet.
     */
    public int creerSommet(T contenu){
        Sommet nouvSommet = new Sommet(contenu);
        this.sommets.add(nouvSommet);
        return nouvSommet.label;
    }

    /**
     * Crée une nouvelle arête entre deux sommets et retourne son label.
     * @param poids Poids de l'arête.
     * @param labelDebut Label du sommet de départ.
     * @param labelFin Label du sommet d'arrivée.
     * @param oriente Si le sommet est orienté ou non.
     * @return Label de l'arête.
     */
    public int creerArete(double poids, int labelDebut, int labelFin, boolean oriente){
        Sommet sommetDebut = this.getSommet(labelDebut);
        Sommet sommetFin = this.getSommet(labelFin);

        Arete nouvArete = new Arete(poids,sommetDebut,sommetFin,oriente);
        this.aretes.add(nouvArete);
        return nouvArete.label;
    }

    /**
     * Retourne le sommet correspondant au label fourni.
     * @param label Label du sommet.
     * @return Le sommet en question s'il existe.
     */
    protected Sommet getSommet(int label){
        for(Sommet sommet : this.sommets){
            if(sommet.label == label){
                return sommet;
            }
        }

        return null;
    }

    /**
     * Obtenir le contenu d'un sommet par son label.
     * @param label Le label du sommet.
     * @return Le contenu du sommet.
     */
    public T getContenuSommet(int label){
        return this.getSommet(label).contenu;
    }

    /**
     * Retourne l'arête correspondant au label fourni.
     * @param label Le label de l'arête.
     * @return L'arête en question si elle existe.
     */
    protected Arete getArete(int label){
        for(Arete arete : this.aretes){
            if(arete.label == label){
                return arete;
            }
        }

        return null;
    }

    /**
     * Supprime l'arête correspondant au label fourni.
     * @param label Le label de l'arête.
     */
    public void supArete(int label){
        Arete aSupp = this.getArete(label);

        if(aSupp != null){
            for(int i = 0; i < this.aretes.size(); i++){
                if(this.aretes.get(i) == aSupp){
                    this.aretes.remove(i).close();
                    break;
                }
            }
        }
    }

    /**
     * Supprime le sommet correspondant au label fourni.
     * @param label Le label du sommet.
     */
    public void supSommet(int label){
        Sommet aSupp = this.getSommet(label);

        if(aSupp != null){
            for(int i = 0; i < this.sommets.size(); i++){
                if(this.sommets.get(i) == aSupp){
                    this.sommets.remove(i).close();
                    break;
                }
            }
        }
    }

    /**
     * Identifie et retourne les composantes connexes du graphe.
     * @return Tableau de graphes contenant les composantes connexes.
     */
    public BonusGraphe<T>[] composantesConnexes() {
        ArrayList<BonusGraphe<T>>composantesConnexes = new ArrayList<>();

        ArrayList<Sommet> copieSommet = new ArrayList<>();
        for(Sommet sommet : this.sommets){
            copieSommet.add(sommet);
        }

        int dernierIndex = copieSommet.size() - 1;
        while(dernierIndex >= 0){
            BonusGraphe<T> composanteConnexe = new BonusGraphe<>();
            composanteConnexe.prochaineArete = this.prochaineArete;
            composanteConnexe.prochainSommet = this.prochainSommet;

            // Addition récursive des sommets et des arêtes d'une même composante connexe.
            composanteConnexe.ajSommet(copieSommet.remove(dernierIndex--), null);

            for(Sommet sommet : composanteConnexe.sommets){
                for(int i = 0; i < copieSommet.size(); i++){
                    if(copieSommet.get(i) == sommet){
                        copieSommet.remove(i--);
                        dernierIndex--;
                    }
                }
            }

           composantesConnexes.add(composanteConnexe);
        }

        BonusGraphe<T>[] ret = new BonusGraphe[composantesConnexes.size()];

        for(int i = 0; i < composantesConnexes.size(); i++){
            ret[i] = composantesConnexes.get(i);
        }

        return ret;
    }

    /**
     * Ajoute un sommet et ses arêtes au graphe.
     * @param aAjout Le sommet à ses arêtes.
     * @param source Arête qui a appelé la méthode.
     */
    private void ajSommet(Sommet aAjout, Arete source){
        boolean unique = true;
        int dernierIndex = this.sommets.size()-1;
        for(int i = dernierIndex; i >= 0; i--){
            if(this.sommets.get(i) == aAjout){
                unique = false;
                break;
            }
        }

        if(unique){
            this.sommets.add(aAjout);
            for(Arete arete : aAjout.aretes){
                if(arete != source){
                    this.ajArete(arete, aAjout);
                }
            }
        }
    }

    /**
     * Ajoute une arête et ses sommets au graphe.
     * @param aAjout L'arête et ses sommets.
     * @param source Sommet qui a appelé la méthode.
     */
    private void ajArete(Arete aAjout, Sommet source){
        boolean unique = true;
        int dernierIndex = this.aretes.size()-1;
        for(int i = dernierIndex; i >= 0; i--){
            if(this.aretes.get(i) == aAjout){
                unique = false;
                break;
            }
        }

        if(unique){
            this.aretes.add(aAjout);
            this.ajSommet(aAjout.getVoisin(source), aAjout);
        }
    }

    /**
     * Indique si le sommet ayant le bon label est contenu dans le graphe.
     * @param requete Le label à rechercher.
     * @return Vrai si le sommet ayant le bon label est contenu dans le graphe.
     */
    public boolean aPourSommet(int requete){
        return !(this.getSommet(requete) == null);
    }

    /**
     * Vérifie si deux sommets sont connectés via une arète. Tient compte de l'orientation.
     * @param labelDebut Le label du sommet dont provient l'arête.
     * @param labelFin Le label du sommet où se rend l'arête.
     * @return Vrai si les deux sommets sont connectés en tenant compte de l'orientation.
     */
    public boolean connectes(int labelDebut, int labelFin){
        Sommet sommetDebut = this.getSommet(labelDebut);
        Sommet sommetFin = this.getSommet(labelFin);

        for(Arete arete : sommetDebut.aretes){
            if(arete.connecte(sommetDebut,sommetFin)){
                return true;
            }
        }

        return false;
    }
}
