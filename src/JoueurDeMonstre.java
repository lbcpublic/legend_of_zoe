public class JoueurDeMonstre implements Runnable {
    private Monstre monstre;
    private int[] zoePos;

    public JoueurDeMonstre(Monstre monstre, int[] zoePos){
        this.monstre = monstre;
        this.zoePos = zoePos;
    }

    @Override
    public void run() {
        if(this.monstre.enVie()){
            this.monstre.attaquer();
            this.monstre.bouger(this.zoePos[0], this.zoePos[1]);
        }
    }
}
