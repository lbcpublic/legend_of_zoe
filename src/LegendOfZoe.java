// IFT1025, TP1
// Par :
// Laurent Bruneau-Cossette
// Raphaël Aubin

import java.util.Scanner;

/**
 * Classe principale du programme.
 *
 * NOTEZ : VOUS NE DEVEZ PAS RENOMMER CETTE CLASSE
 */
public class LegendOfZoe {
    private static Zoe persPrinc;
    private static Niveau nivActuel;
    private static int numNivActu;
    private static boolean monstresIntelligents = false;

    /**
     * Jeu actuel. Instancie d'abord Zoé comme persPrinc et le numéro de niveau à zéro
     * pour permettre aux autres classes de faire leur travail. Initialise ensuite un
     * scanner qui prendra en entrée les actions à accomplir dans le niveau. Créera un
     * nouveau niveau quand les conditions nécessaires seront rencontrées.
     * @param args Définit les modes de jeux. "--god" pour déboguer sans perdre de vie
     *             ou "--bonus" pour des monstres toujours mobiles.
     */
    public static void main(String[] args) {
        Messages.afficherIntro();

        for(String arg : args){
            if(arg.equals("--god")){
                // God mode, pour déboguer ou tricher, selon ce qui vous plait.
                System.out.println("Invincibilité activée. Si ce n'est pas " +
                        "pour déboguer, vous êtes un tricheur!");
                Personnage.godMode();
            }else if(arg.equals("--bonus")){
                // Mode difficile (bonus)
                System.out.println("Attention! Les monstres sont plus astucieux!");
                monstresIntelligents = true;
            }
        }

        persPrinc = new Zoe();

        Consommable.setPersPrinc(persPrinc);
        Niveau.setPersPrinc(persPrinc);

        Scanner scanner = new Scanner(System.in);

        numNivActu = 0;

        creerProchainNiveau();

        String entree;

        while(true){
            System.out.println(nivActuel);

            entree = scanner.nextLine();

            for(int i = 0; i < entree.length(); i++){
                jouerZoe(entree.charAt(i));

                if(nivActuel.estComplete()){
                    System.out.println(nivActuel);
                    creerProchainNiveau();
                    persPrinc.reinitHexaforce();
                    break;
                }

                nivActuel.jouerMonstres();
            }
        }
    }

    /**
     * Procédure utilisant les méthodes de Zoé selon les entrées au clavier.
     * @param i "caractère" corespondant à l'action de Zoé.
     */
    private static void jouerZoe(char i){
        switch (i){
            case 'w':{
                persPrinc.bouger(0,-1);
                break;
            }
            case 'a':{
                persPrinc.bouger(-1,0);
                break;
            }
            case 's':{
                persPrinc.bouger(0,1);
                break;
            }
            case 'd':{
                persPrinc.bouger(1,0);
                break;
            }
            case 'c':{
                persPrinc.creuser();
                break;
            }
            case 'x':{
                persPrinc.attaquer();
                break;
            }
            case 'o':{
                persPrinc.ouvrirCoffres();
                break;
            }
            case 'q':{
                System.exit(0);
                break;
            }
        }
    }

    /**
     * Création d'un nouveau niveau.
     */
    private static void creerProchainNiveau(){
        Paire<boolean[][], String[]> infoNivActu =
                LevelGenerator.generateLevel(++numNivActu);
        nivActuel = new Niveau(infoNivActu.getKey(),
                infoNivActu.getValue(), numNivActu, monstresIntelligents);
        Positionnable.setNiveau(nivActuel);
    }

    /**
     * Affiche l'écran de victoire et termine le jeu. Déclenchée par Zoé lorsqu'elle a tous
     * les hexaforces.
     */
    public static void victoire(){
        System.out.println(nivActuel);
        Scanner scanner = new Scanner(System.in);
        Messages.afficherVictoire();
        System.out.println("Appuyez sur Entrée pour terminer...");
        scanner.nextLine();
        System.exit(0);
    }

    /**
     * Affiche l'écran de victoire et termine le jeu. Déclenchée par Zoé lorsqu'elle a tous
     * les hexaforces.
     */
    public static void defaite(){
        System.out.println(nivActuel);
        Scanner scanner = new Scanner(System.in);
        Messages.afficherDefaite();
        System.out.println("Appuyez sur Entrée pour terminer...");
        scanner.nextLine();
        System.exit(0);
    }
}
