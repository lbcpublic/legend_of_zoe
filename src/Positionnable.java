/**
 * Classe mère de tous les objets qui ont une position sur la carte de jeu.
 * C'est elle qui permettra de positionner les éléments lors de la création
 * de niveau, et qui permettra d'ajuster leur position au fil des tours.
 */
public abstract class Positionnable {
    protected int[] pos;
    protected static Niveau lvl;

    /**
     * Constructeur qui indique que la position en X et en Y sera sous forme
     * de tableau de deux données.
     */
    public Positionnable(){
        this.pos = new int[2];
    }

    /**
     * "Getter" qui retourne la position d'un objet.
     * @return le tableau qui contient la position en X et en Y d'un objet.
     */
    public final int[] getPos(){
        int[] copiePos = new int[2];
        copiePos[0] = this.pos[0];
        copiePos[1] = this.pos[1];
        return copiePos;
    }

    /**
     * "Setter" qui assigne la position d'un objet.
     * @param pos un tableau contenant la position en X et en Y d'un objet
     *            à positionner.
     */
    public final void setPos(int[] pos){
        this.pos[0] = pos[0];
        this.pos[1] = pos[1];
    }

    /**
     * Assigne le tableau actuel du jeu, puisque les positions sont en relations
     * avec le niveau actuel.
     * @param lvl le niveau actuel du jeu.
     */
    public static final void setNiveau(Niveau lvl){
        Positionnable.lvl = lvl;
    }
}